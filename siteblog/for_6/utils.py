from pymongo import MongoClient
import pymongo


def get_db_handle(db_name, host, port, username, password):
    client = MongoClient(host=host,
                         port=int(port),
                         username=username,
                         password=password,
                         authSource="admin")

    print('client', client)
    db_handle = client[db_name]
    return db_handle, client


def get_collection_handle(db_handle, collection_name):
    return db_handle[collection_name]


client = get_db_handle('meteodata', '127.0.0.1', 27017, 'admin', 'mypwd')[1]

collection = client.meteodata
