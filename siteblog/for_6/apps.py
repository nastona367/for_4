from django.apps import AppConfig


class For6Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'for_6'
