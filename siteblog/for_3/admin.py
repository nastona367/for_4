from django import forms
from django.contrib import admin
from .models import *


class GenshinAdmin(admin.ModelAdmin):
    list_display = ('name', 'color', 'arm', 'ataka', 'hp')


admin.site.register(GenshinCharecters, GenshinAdmin)
