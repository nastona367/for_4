from django.db import models


class GenshinCharecters(models.Model):
    name = models.CharField(max_length=20,verbose_name="Ім'я героя")
    color = models.CharField(max_length=20, verbose_name="Стехія")
    arm = models.CharField(max_length=20, verbose_name="Зброя")
    ataka = models.IntegerField(verbose_name="Сила атаки")
    hp = models.IntegerField(verbose_name="HP")
