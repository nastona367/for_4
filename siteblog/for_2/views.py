from django.shortcuts import render
from django.http import HttpResponseRedirect


def some(request):
    return FileResponse(open(static('file.txt'), "rb+"), )


def main(request):
    return render(request, 'pages/main.html')
