from django.apps import AppConfig


class For2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'for_2'
